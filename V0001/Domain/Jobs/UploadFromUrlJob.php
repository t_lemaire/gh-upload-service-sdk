<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

/**
 * Class UploadFromUrlJob
 * @package GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs
 */
class UploadFromUrlJob implements SDKCallInterface
{

    /**
     * @var Signature
     */
    private $signature;

    /**
     * @var string
     */
    private $callbackUrl;

    /**
     * @var string
     */
    private $fileUrl;

    /**
     * @var bool $allowMultipleDownload
     */
    private $allowMultipleDownload = false;

    /**
     * @var mixed
     */
    private $custom;

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return self
     */
    public function setSignature(Signature $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $callbackUrl
     *
     * @return self
     */
    public function setCallbackUrl($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

    /**
     * @return string
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setFileUrl($url)
    {
        $this->fileUrl = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustom()
    {
        return $this->custom;
    }

    /**
     * @param mixed $custom
     *
     * @return $this
     */
    public function setCustom($custom)
    {
        $this->custom = $custom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAllowMultipleDownload()
    {
        return $this->allowMultipleDownload;
    }

    /**
     * Allow multiple download of this URL means to the upload service that it will use this URL for every time
     * the file is needed
     * @param mixed $allowMultipleDownload
     */
    public function setAllowMultipleDownload($allowMultipleDownload)
    {
        $this->allowMultipleDownload = $allowMultipleDownload;
    }


}