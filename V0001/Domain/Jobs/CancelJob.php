<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

class CancelJob implements SDKCallInterface
{

	/**
	 * @var Signature
	 */
	private $signature;

	/**
	 * @var bool
	 */
	private $deleteOriginal = false;

	/**
	 * @var string
	 */
	private $callbackUrl;

	/**
	 * Sets the signature.
	 *
	 * @param Signature $signature
	 *
	 * @return self
	 */
	public function setSignature(Signature $signature)
	{
		$this->signature = $signature;

		return $this;
	}

	/**
	 * Returns the signature.
	 *
	 * @return Signature
	 */
	public function getSignature()
	{
		return $this->signature;
	}

	/**
	 * @param boolean $deleteOriginal
	 */
	public function setDeleteOriginal($deleteOriginal)
	{
		$this->deleteOriginal = $deleteOriginal;
	}

	/**
	 * @return boolean
	 */
	public function getDeleteOriginal()
	{
		return $this->deleteOriginal;
	}

	/**
	 * @param string $callbackUrl
	 */
	public function setCallbackUrl($callbackUrl)
	{
		$this->callbackUrl = $callbackUrl;
	}

	/**
	 * @return string
	 */
	public function getCallbackUrl()
	{
		return $this->callbackUrl;
	}



}