<?php
namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Image;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Job;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\DateTime;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * Class ImageUploadJob
 * @Annotation
 * @package GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Image
 */
class ImageUploadJob extends Job
{
	/**
	 * @var string
	 */
	protected $callbackUrl;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->setDateTime(new DateTime());
	}

	/**
	 * Sets the url to call back the site after receiving the response from the image server.
	 *
	 * @param $url
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setCallbackUrl($url)
	{
		if (mb_strpos($url, 'http://') === 0 || mb_strpos($url, 'https://') === 0) {
			$this->callbackUrl = $url;
		} else {
			throw new InvalidParameterException('Invalid callback url format.');
		}

		return $this;
	}

	/**
	 * Returns the call back url.
	 *
	 * @return string
	 */
	final public function getCallbackUrl()
	{
		return $this->callbackUrl;
	}

}