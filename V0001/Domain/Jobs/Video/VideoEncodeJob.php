<?php
namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Video;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\VideoJob;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters\ValidConditionsParameter;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\DateTime;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * Class VideoEncodeJob
 *
 * @Annotation
 *
 * @package GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Video
 */
class VideoEncodeJob extends VideoUploadJob
{



}