<?php
namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Video;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\VideoJob;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters\ValidConditionsParameter;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\SaferQueryOperation;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\DetectCSAIOperation;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\EncodeOperation;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\DateTime;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * Class VideoUploadJob
 *
 * @Annotation
 *
 * @package GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Video
 */
class VideoUploadJob extends VideoJob
{

    /**
     * The video download url for the encoder
     *
     * @var string
     */
    private $downloadVideoUrl;

    /**
     * @var string|null The ID of the user who uploaded the file, or null if unknown.  This is used to reduce the
     *      priority of a user once they have uploaded too much.
     */
    private $uploaderUserId = null;

    /**
     * @var int|null The number of megabytes the user is allowed to upload per day, or null if there is no limit.
     */
    private $dailyLimitMB = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setValidConditions(new ValidConditionsParameter());
        $this->setDateTime(new DateTime());
    }

    /**
     * Sets the download video url for the encoder.
     *
     * @param $url
     * @return self
     * @throws InvalidParameterException
     */
    final public function setDownloadVideoUrl($url)
    {
        if (mb_strpos($url, 'http://') === 0 || mb_strpos($url, 'https://') === 0) {
            $this->downloadVideoUrl = $url;
        } else {
            throw new InvalidParameterException('Invalid download video url format.');
        }

        return $this;
    }

    /**
     * Return the download video url
     *
     * @return string
     */
    final public function getDownloadVideoUrl()
    {
        return $this->downloadVideoUrl;
    }

    /**
     * @return string|null The ID of the user who uploaded the file, or null if unknown.  This is used to reduce the
     *      priority of a user once they have uploaded too much.
     */
    public function getUploaderUserId()
    {
        return $this->uploaderUserId;
    }

    /**
     * @param string|null $uploaderUserId The ID of the user who uploaded the file, or null if unknown.  This is used
     *      to reduce the priority of a user once they have uploaded too much.
     * @return $this
     */
    public function setUploaderUserId($uploaderUserId)
    {
        $this->uploaderUserId = $uploaderUserId;
        return $this;
    }

    /**
     * @return int|null The number of megabytes the user is allowed to upload per day, or null if there is no limit.
     */
    public function getDailyLimitMB()
    {
        return $this->dailyLimitMB;
    }

    /**
     * @param int|null $dailyLimitMB The number of megabytes the user is allowed to upload per day, or null if there
     *      is no limit.
     * @return $this
     */
    public function setDailyLimitMB($dailyLimitMB)
    {
        $this->dailyLimitMB = $dailyLimitMB;
        return $this;
    }

    /**
     * @return int|null The number of bytes the user is allowed to upload per day, or null if there is no limit.
     */
    public function getDailyLimitBytes() {
        return ($this->dailyLimitMB !== null) ? $this->dailyLimitMB * 1024 * 1024 : null;
    }



    /**
     * @inheritdoc
     */
    public function addOperation(OperationInterface $operation)
    {
        if ($operation instanceof VideoOperation && !($operation instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface)) {
            if (null == $operation->getFilePattern()) {
                throw new InvalidParameterException('Invalid operation content, File Pattern definition is missing for operation: ' . get_class($operation) . ', id:' . $operation->getOperationId());
            }

            if ($this->filePatternExists($operation->getFilePattern()) === true) {
                throw new InvalidParameterException('The file pattern definition has to be unique in the job.');
            }
        }

        parent::addOperation($operation);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setOperations(array $operations)
    {
        $this->operations = array();

        foreach ($operations as $operation) {
            if ($operation instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface) {
                $this->addOperation($operation);
            }
        }

        return $this;
    }


    /**
     * This calls $filterCallback once per operation, passing it the operation as the first and only argument.
     * If the function returns true, the operation is kept.
     *
     * @param callable $filterCallback
     */
    public function filterOperations($filterCallback) {
        $this->operations = array_values(
            array_filter($this->operations, $filterCallback)
        );
    }


    /**
     * @return DetectCSAIOperation The first Detect CSAI operation, or null if none.
     */
    public function getDetectCSAIOperation() {
        foreach ($this->operations as $operation) {
            if ($operation instanceof DetectCSAIOperation) {
                return $operation;
            }
        }
        return null;
    }

    /**
     * @return SaferQueryOperation The first SaferQueryOperation, or null if none.
     */
    public function getSaferQueryOperation() {
        foreach ($this->operations as $operation) {
            if ($operation instanceof SaferQueryOperation) {
                return $operation;
            }
        }
        return null;
    }

    /**
     * @return EncodeOperation[] All video encode operations.
     */
    public function getEncodeOperations()
    {
        $operations = [];
        foreach ($this->operations as $operation) {
            if ($operation instanceof EncodeOperation) {
                $operations[] = $operation;
            }
        }
        return $operations;
    }

    /**
     * Verify if the passed fieldPattern match any existing VideoOperation file pattern
     *
     * @param FilePattern $filePattern
     *
     * @return bool
     */
    private function filePatternExists(FilePattern $filePattern)
    {
        $filePatternString = (string) $filePattern;

        foreach ($this->getOperations() as $operation) {
            if ($operation instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePatternInterface ) {
                if ($filePatternString === (string) $operation->getFilePattern()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * This sets the encoder tag of the job and all operations.
     *
     * @param string $encoderTag
     */
    public function setAllEncoderTags($encoderTag)
    {
        $this->setEncoderTag($encoderTag);
        foreach ($this->operations as $operation) {
            if ($operation instanceof Operation) {
                $operation->setEncoderTag($encoderTag);
            }
        }
    }


}
