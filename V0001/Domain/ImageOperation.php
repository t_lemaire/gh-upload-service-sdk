<?php
/**
 * Created by PhpStorm.
 * User: e_pickup
 * Date: 11/02/14
 * Time: 10:39 AM
 */

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\ImageBasedOperation;


class ImageOperation extends ImageBasedOperation
{

	/**
	 * @var string
	 */
	private $fileName = '';

	/**
	 * @param string $fileName
	 */
	public function setFileName($fileName)
	{
		$this->fileName = $fileName;
	}

	/**
	 * @return string
	 */
	public function getFileName()
	{
		return $this->fileName;
	}
}