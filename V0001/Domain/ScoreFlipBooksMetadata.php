<?php
namespace GorillaHub\SDKs\UploadBundle\V0001\Domain;

/**
 * An instance of this class remembers additional data about a video that can be used to help score its thumbnails.
 */
class ScoreFlipBooksMetadata
{
	/** @var string The ID of the video. */
	private $videoId;

	/** @var string One of "straight", "gay", "shemale". */
	private $orientation;

	/** @var string One of "homemade" or "professional". */
	private $production;

	/** @var string[] Zero or more categories in which the video appears. */
	private $categories = [];

	/** @var string[] Zero or more search tags associated with video. */
	private $tags = [];

	/** @var string[] Zero or more porn stars that are featured in the video. */
	private $pornstars = [];

	/**
	 * @return string @see $videoId
	 */
	public function getVideoId()
	{
		return $this->videoId;
	}

	/**
	 * @param string|int $videoId @see $videoId
	 * @return $this
	 */
	public function setVideoId($videoId)
	{
		$this->videoId = (string)$videoId;
		return $this;
	}

	/**
	 * @return string @see $orientation
	 */
	public function getOrientation()
	{
		return $this->orientation;
	}

	/**
	 * @param string $orientation @see $orientation
	 * @return $this
	 */
	public function setOrientation($orientation)
	{
		$this->orientation = $orientation;
		return $this;
	}

	/**
	 * @return string @see $production
	 */
	public function getProduction()
	{
		return $this->production;
	}

	/**
	 * @param string $production @see $production
	 * @return $this
	 */
	public function setProduction($production)
	{
		$this->production = $production;
		return $this;
	}

	/**
	 * @return string[] @see $categories
	 */
	public function getCategories()
	{
		return $this->categories;
	}

	/**
	 * @param string[] $categories @see $categories
	 * @return $this
	 */
	public function setCategories($categories)
	{
		$this->categories = $categories;
		return $this;
	}

	/**
	 * @return string[] @see $tags
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * @param string[] $tags @see $tags
	 * @return $this
	 */
	public function setTags($tags)
	{
		$this->tags = $tags;
		return $this;
	}

	/**
	 * @return string[] @see $pornstars
	 */
	public function getPornstars()
	{
		return $this->pornstars;
	}

	/**
	 * @param string[] $pornstars @see $pornstars
	 * @return $this
	 */
	public function setPornstars($pornstars)
	{
		$this->pornstars = $pornstars;
		return $this;
	}



}