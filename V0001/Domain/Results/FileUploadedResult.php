<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Results;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;
use GorillaHub\SDKs\UploadBundle\Utils\Base64Encoder;
use \MindGeek\MediaInfoBundle\Domain\FileInfo;

class FileUploadedResult extends Result
{

	/**
	 * @var array
	 */
	private $post;

	/**
	 * @var array
	 */
	private $cookies;

	/**
	 * @var array
	 */
	private $get;

	/**
	 * @var string
	 */
	private $fileHash;

	/**
	 * @var FileInfo
	 */
	private $fileInfo;

	/**
	 * @var string
	 */
	private $downloadableAt = '';

	/**
	 * @var int
	 */
	private $fileSize = 0;

	/**
	 * @var string
	 */
	private $fileExtension = '';


	/**
	 * @param array $cookies
	 *
	 * @return $this
	 */
	final public function setCookies($cookies)
	{
		$this->cookies = $cookies;

		return $this;
	}

	/**
	 * @return array
	 */
	final public function getCookies()
	{
		return $this->cookies;
	}

	/**
	 * @param string $fileHash
	 *
	 * @return $this
	 */
	final public function setFileHash($fileHash)
	{
		$this->fileHash = $fileHash;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getFileHash()
	{
		return $this->fileHash;
	}

	/**
	 * @param array $post
	 *
	 * @return $this
	 */
	final public function setPost($post)
	{
		$this->post = $post;

		return $this;
	}

	/**
	 * @return array
	 */
	final public function getPost()
	{
		return $this->post;
	}

	/**
	 * @param array $get
	 *
	 * @return $this
	 */
	final public function setGet($get)
	{
		$this->get = $get;

		return $this;
	}

	/**
	 * @return array
	 */
	final public function getGet()
	{
		return $this->get;
	}

	/**
	 * @param FileInfo $fileInfo
	 *
	 * @return $this
	 */
	public function setFileInfo(FileInfo $fileInfo)
	{
		$this->fileInfo = $fileInfo;

		return $this;
	}

	/**
	 * @return FileInfo
	 */
	public function getFileInfo()
	{
		return $this->fileInfo;
	}

	/**
	 * @param string $downloadable
	 *
	 * @return $this
	 */
	public function setDownloadableAt($downloadable)
	{
		$this->downloadableAt = $downloadable;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getDownloadableAt()
	{
		return $this->downloadableAt;
	}

	/**
	 * @param string $fileExtension
	 *
	 * @return $this
	 */
	public function setFileExtension($fileExtension)
	{
		$this->fileExtension = $fileExtension;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFileExtension()
	{
		return $this->fileExtension;
	}

	/**
	 * @param int $fileSize
	 *
	 * @return $this
	 */
	public function setFileSize($fileSize)
	{
		$this->fileSize = $fileSize;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getFileSize()
	{
		return $this->fileSize;
	}

}