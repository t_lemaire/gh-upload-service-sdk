<?php
namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Results;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

class PhotoDNAResult
{
    /** @var Signature */
    private $signature;

    /**
     * @var bool True if the image was a match, false if it was not a match.
     */
    public $isMatch;

    /** @var string An identifier for this particular request assigned by the PhotoDNA service. */
    public $trackingId;

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return self
     */
    public function setSignature(Signature $signature) {
        $this->signature = $signature;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature() {
        return $this->signature;
    }

    /**
     * @return boolean True if the image was a match, false if it was not a match.
     */
    public function getIsMatch()
    {
        return $this->isMatch;
    }

    /**
     * @param boolean $isMatch True if the image was a match, false if it was not a match.
     * @return $this
     */
    public function setIsMatch($isMatch)
    {
        $this->isMatch = $isMatch;
        return $this;
    }

    /**
     * @return string An identifier for this particular request assigned by the PhotoDNA service.
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * @param string $trackingId An identifier for this particular request assigned by the PhotoDNA service.
     * @return $this
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;
        return $this;
    }



}
