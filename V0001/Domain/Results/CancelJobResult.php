<?php


namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Results;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

class CancelJobResult extends Result
{

	/**
	 * @var \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\DeleteRequestResult $deleteRequestResult
	 */
	private $deleteRequestResult;

	/**
	 * @param \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\DeleteRequestResult $deleteRequestResult
	 */
	public function setDeleteRequestResult($deleteRequestResult)
	{
		$this->deleteRequestResult = $deleteRequestResult;
	}

	/**
	 * @return \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\DeleteRequestResult
	 */
	public function getDeleteRequestResult()
	{
		return $this->deleteRequestResult;
	}


} 