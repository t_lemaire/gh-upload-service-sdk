<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Operations;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePatternInterface;

class StoreInTheOriginOperation extends Operation implements FilePatternInterface
{

	/**
	 * @var FilePattern
	 */
	protected $filePattern;

	/**
	 * @param FilePattern $filePattern
	 *
	 * @return $this
	 */
	final public function setFilePattern(FilePattern $filePattern)
	{
		$this->filePattern = $filePattern;

		return $this;
	}

	/**
	 * @return FilePattern
	 */
	final public function getFilePattern()
	{
		return $this->filePattern;
	}

	public function autoSelectVolumeName() {
		if ($this->getVolumeName() !== null) {
			return $this->getVolumeName();
		}
		if ($this->getFilePattern() !== null) {
			if ($this->getFilePattern()->getVolumeName() !== null) {
				return $this->getFilePattern()->getVolumeName();
			}
		}
		return 'default';
	}



}