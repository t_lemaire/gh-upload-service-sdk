<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Operations;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePatternInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;

class StoreInTheOriginWithCallbackOperation extends StoreInTheOriginOperation implements GenerateCallBackInterface
{
}