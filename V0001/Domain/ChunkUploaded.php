<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain;

/**
 * Class ChunkUploaded
 * @package GorillaHub\SDKs\UploadBundle\V0001\Domain
 */
class ChunkUploaded
{

	/**
	 * @var string
	 */
	protected $fileId = '';

	/**
	 * @var
	 */
	protected $fileName;

	/**
	 * @var int
	 */
	protected $rangeStartsAt = 0;

	/**
	 * @var int
	 */
	protected $rangeEndsAt = 0;

	/**
	 * @var int
	 */
	protected $contentLength = 0;

	/**
	 * @var int
	 */
	protected $fileSize = 0;

	/**
	 * @return string
	 */
	public function getFileId()
	{
		return $this->fileId;
	}

	/**
	 * @param string $fileId
	 *
	 * @return $this
	 */
	public function setFileId($fileId)
	{
		$this->fileId = $fileId;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFileName()
	{
		return $this->fileName;
	}

	/**
	 * @param mixed $fileName
	 *
	 * @return $this
	 */
	public function setFileName($fileName)
	{
		$this->fileName = $fileName;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getRangeStartsAt()
	{
		return $this->rangeStartsAt;
	}

	/**
	 * @param int $rangeStartsAt
	 *
	 * @return $this
	 */
	public function setRangeStartsAt($rangeStartsAt)
	{
		$this->rangeStartsAt = $rangeStartsAt;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getRangeEndsAt()
	{
		return $this->rangeEndsAt;
	}

	/**
	 * @param int $rangeEndsAt
	 *
	 * @return $this
	 */
	public function setRangeEndsAt($rangeEndsAt)
	{
		$this->rangeEndsAt = $rangeEndsAt;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getContentLength()
	{
		return $this->contentLength;
	}

	/**
	 * @param int $contentLength
	 *
	 * @return $this
	 */
	public function setContentLength($contentLength)
	{
		$this->contentLength = $contentLength;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getFileSize()
	{
		return $this->fileSize;
	}

	/**
	 * @param int $fileSize
	 *
	 * @return $this
	 */
	public function setFileSize($fileSize)
	{
		$this->fileSize = $fileSize;

		return $this;
	}

}