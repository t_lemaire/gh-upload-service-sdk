<?php
namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Calls;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

/**
 * A call of this type is sent to the Upload Service to create a Photo DNA job.  When the job is done, the client
 * is called back at their subscription callback URL (even if a subscription is not requested).
 */
class CreatePhotoDNAJobCall implements SDKCallInterface
{
    /** @var Signature */
    private $signature;

    /**
     * @var string|null A custom ID that is returned with the PhotoDNAResultCall.  You can use this to figure
     *      out which photo the PhotoDNAResultCall relates to.  This is optional; you can use the job ID
     *      specified in the Signature for this purpose instead.
     */
    private $customId = null;

    /**
     * @var string The URL of the file.  You may specify a URL of the form "file://volume/path/to/file", where
     *      "volume" is the name of the volume (e.g. "c1234", or a storage alias, or "default") and /path/to/file
     *      is relative to your home folder.  The volume may be empty (e.g. "file:///path/to/file") in which case
     *      it is taken to be "default".
     */
    private $photoUrl;

    /**
     * @var bool True if the photo should be re-checked periodically.  In this case, there could be multiple
     *      callbacks for this job to the Subscription job.
     */
    private $subscribe;

    /**
     * @var int|null The unix timestamp of the time at which the photo was first uploaded to the site, or the time at
     *      which it was created, or null if not known.  This is useful if $subscribe is true; if the file is old,
     *      we do not re-scan as frequently.
     */
    private $mediaTime;

    /**
     * @var string One of the Priorities::ENCODER_TAG_* tags.
     */
    private $priority;

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return self
     */
    public function setSignature(Signature $signature) {
        $this->signature = $signature;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature() {
        return $this->signature;
    }

    /**
     * @return null|string @see $customId
     */
    public function getCustomId() {
        return $this->customId;
    }

    /**
     * @param null|string $customId @see $customId
     * @return $this
     */
    public function setCustomId($customId) {
        $this->customId = $customId;
        return $this;
    }



    /**
     * @return string  @see $photoUrl
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    /**
     * @param string $photoUrl The URL of the file.  You may specify a URL of the form
     *      "file://volume/path/to/file", where "volume" is the name of the volume (e.g. "c1234", or a storage
     *      alias, or "default") and /path/to/file is relative to your home folder.  The volume may be empty
     *      (e.g. "file:///path/to/file") in which case it is taken to be "default".
     * @return $this
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;
        return $this;
    }

    /**
     * @return boolean  @see $subscribe
     */
    public function getSubscribe()
    {
        return $this->subscribe;
    }

    /**
     * @param boolean $subscribe @see $subscribe
     * @return $this
     */
    public function setSubscribe($subscribe)
    {
        $this->subscribe = $subscribe;
        return $this;
    }

    /**
     * @return int|null  @see $mediaTime
     */
    public function getMediaTime()
    {
        return $this->mediaTime;
    }

    /**
     * @param int|null $mediaTime @see $mediaTime
     * @return $this
     */
    public function setMediaTime($mediaTime)
    {
        $this->mediaTime = $mediaTime;
        return $this;
    }

    /**
     * @return string @see $priority
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param string $priority @see $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }



}
