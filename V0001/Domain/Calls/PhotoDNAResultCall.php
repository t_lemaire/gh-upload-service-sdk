<?php
namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Calls;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use GorillaHub\SDKs\UploadBundle\V0001\Domain\Results\PhotoDNAResult;

/**
 * A call of this type is sent to the client's subscription URL when a Photo DNA job finishes, or when it is rescanned
 * at a later time.
 */
class PhotoDNAResultCall implements SDKCallInterface
{
    /** @var Signature */
    private $signature;

    /** @var string|null If something went wrong, this is a message indicating what happened. */
    private $errorMessage = null;

    /** @var string|null The raw response from the Photo DNA service, or null if none. */
    private $rawResponse = null;

    /** @var string|null The custom ID assigned to the original PhotoDNAJobCall. */
    private $customId = null;

    /**
     * @var PhotoDNAResult|null The parsed result from the Photo DNA service, or null if none.  This field is null
     *      iff $errormessage is not null.
     */
    private $photoDNAResult = null;

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return self
     */
    public function setSignature(Signature $signature) {
        $this->signature = $signature;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature() {
        return $this->signature;
    }

    /**
     * @return null|string  @see $errorMessage
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param null|string $errorMessage @see $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return null|string  @see $rawResponse
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * @param null|string $rawResponse @see $rawResponse
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
    }

    /**
     * @return null|string @see $customId
     */
    public function getCustomId() {
        return $this->customId;
    }

    /**
     * @param null|string $customId
     * @return PhotoDNAResultCall
     */
    public function setCustomId($customId) {
        $this->customId = $customId;
        return $this;
    }


    /**
     * @return PhotoDNAResult  @see $photoDNAResult
     */
    public function getPhotoDNAResult()
    {
        return $this->photoDNAResult;
    }

    /**
     * @param PhotoDNAResult|null $photoDNAResult @see $photoDNAResult
     */
    public function setPhotoDNAResult($photoDNAResult)
    {
        $this->photoDNAResult = $photoDNAResult;
    }





}
