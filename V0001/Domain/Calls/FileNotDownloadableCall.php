<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Calls;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Calls\Call;

/**
 * Class FileNotDownloadableCall
 * @package GorillaHub\SDKs\UploadBundle\V0001\Domain\Calls
 */
class FileNotDownloadableCall extends Call
{

	/**
	 * @var string
	 */
	protected $reason = '';

	/**
	 * @return string
	 */
	public function getReason()
	{
		return $this->reason;
	}

	/**
	 * @param string $reason
	 *
	 * @return $this
	 */
	public function setReason($reason)
	{
		$this->reason = $reason;

		return $this;
	}

}