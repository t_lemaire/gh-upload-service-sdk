<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Calls;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Calls\Call;

class FileUploadedCall extends Call
{

	/**
	 * @var string
	 */
	private $callbackUrl;

	/**
	 * @param string $url
	 *
	 * @return $this
	 */
	public function setCallbackUrl($url)
	{
		$this->callbackUrl = $url;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCallbackUrl()
	{
		return $this->callbackUrl;
	}

} 