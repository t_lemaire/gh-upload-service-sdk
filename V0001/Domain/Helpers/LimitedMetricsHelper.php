<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain\Helpers;

use \Mindgeek\MediaInfoBundle\Domain\MediaInfo;
use \Mindgeek\MediaInfoBundle\Domain\MediaInfo\VideoInfo;

/**
 * Class LimitedMetrics
 *
 * This class is used to get the metrics at which to encode a video after they have been limited
 * to the metrics of the original video.
 *
 * @package GorillaHub\SDKs\UploadBundle\V0001\Domain\Helpers
 */
class LimitedMetricsHelper
{

	/**
	 * @var \Mindgeek\MediaInfoBundle\Domain\MediaInfo The media info of the original video
	 */
	private $originalMediaInfo;

	/**
	 * @var \Mindgeek\MediaInfoBundle\Domain\MediaInfo The media info of the desired video
	 */
	private $desiredMediaInfo;

	/**
	 * @var \Mindgeek\MediaInfoBundle\Domain\MediaInfo The media info specifying the minimum metrics of a video that should be encoded
	 */
	private $minimumMediaInfo;

	/**
	 * @param MediaInfo $originalMediaInfo
	 * @param MediaInfo $desiredMediaInfo
	 * @param MediaInfo $minimumMediaInfo
	 */
	public function __construct(MediaInfo $originalMediaInfo, MediaInfo $desiredMediaInfo, MediaInfo $minimumMediaInfo)
	{
		$this->originalMediaInfo = $originalMediaInfo;
		$this->desiredMediaInfo = $desiredMediaInfo;
		$this->minimumMediaInfo = $minimumMediaInfo;
	}

	/**
	 * @return bool
	 */
	public function shouldVideoBeEncoded()
	{
		$originalVideoInfo = $this->originalMediaInfo->getVideoInfo();
		$minimumVideoInfo = $this->minimumMediaInfo->getVideoInfo();

		if ($minimumVideoInfo instanceof VideoInfo) {
			if ($minimumVideoInfo->getHeight() > $originalVideoInfo->getHeight()) {
				return false;
			}

			if ($minimumVideoInfo->getBitRate() > $originalVideoInfo->getBitRate()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns the video bit rate at which the video should be encoded.
	 *
	 * @return int
	 */
	public function getVideoBitRate()
	{
		return min(
			$this->originalMediaInfo->getVideoInfo()->getBitRate(),
			$this->desiredMediaInfo->getVideoInfo()->getBitRate()
		);
	}

	/**
	 * Returns the height at which the video should be encoded
	 *
	 * @return int
	 */
	public function getHeight()
	{
		return min(
			$this->originalMediaInfo->getVideoInfo()->getHeight(),
			$this->desiredMediaInfo->getVideoInfo()->getHeight()
		);
	}

}