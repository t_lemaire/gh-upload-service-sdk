<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain;

/**
 * An instance of this class describes a clip in the uploaded video (the video was uploaded to LITMUS) that matches
 * a clip in an annotated reference video that was found in the CSAI database.
 */
class CSAIMatchingClip
{
    /**
     * @var float|null The start time, in seconds, within the uploaded video of the matching clip, or null if
     *      not known.
     */
    private $uploadedStartTimeSeconds;

    /**
     * @var float|null The end time, in seconds, within the uploaded video of the matching clip, or null if
     *      not known.
     */
    private $uploadedEndTimeSeconds;

    /** @var float The start time, in seconds, of the matching clip within the reference video. */
    private $referenceStartTimeSeconds;

    /** @var float The end time, in seconds, of the matching clip within the reference video. */
    private $referenceEndTimeSeconds;

    /**
     * @var string|null A category name, like "CATEGORY_B1", or null if none.  Presumably this describes the type
     *      of CSAI, but this is undocumented.
     */
    private $category;

    /** @var string|null The signature of the reference video, if provided. */
    private $referenceSignature;

    /** @var string|null The ID of the reference video, if provided. */
    private $referenceVideoId;

    /**
     * @return float|null @see $uploadedStartTimeSeconds
     */
    public function getUploadedStartTimeSeconds()
    {
        return $this->uploadedStartTimeSeconds;
    }

    /**
     * @param float|null $uploadedStartTimeSeconds @see $uploadedStartTimeSeconds
     * @return $this
     */
    public function setUploadedStartTimeSeconds($uploadedStartTimeSeconds)
    {
        $this->uploadedStartTimeSeconds = $uploadedStartTimeSeconds;
        return $this;
    }

    /**
     * @return float|null @see $uploadedEndTimeSeconds
     */
    public function getUploadedEndTimeSeconds()
    {
        return $this->uploadedEndTimeSeconds;
    }

    /**
     * @param float|null $uploadedEndTimeSeconds @see $uploadedEndTimeSeconds
     * @return $this
     */
    public function setUploadedEndTimeSeconds($uploadedEndTimeSeconds)
    {
        $this->uploadedEndTimeSeconds = $uploadedEndTimeSeconds;
        return $this;
    }

    /**
     * @return float @see $referenceStartTimeSeconds
     */
    public function getReferenceStartTimeSeconds()
    {
        return $this->referenceStartTimeSeconds;
    }

    /**
     * @param float $referenceStartTimeSeconds @see $referenceStartTimeSeconds
     * @return $this
     */
    public function setReferenceStartTimeSeconds($referenceStartTimeSeconds)
    {
        $this->referenceStartTimeSeconds = $referenceStartTimeSeconds;
        return $this;
    }

    /**
     * @return float @see $referenceEndTimeSeconds
     */
    public function getReferenceEndTimeSeconds()
    {
        return $this->referenceEndTimeSeconds;
    }

    /**
     * @param float $referenceEndTimeSeconds @see $referenceEndTimeSeconds
     * @return $this
     */
    public function setReferenceEndTimeSeconds($referenceEndTimeSeconds)
    {
        $this->referenceEndTimeSeconds = $referenceEndTimeSeconds;
        return $this;
    }

    /**
     * @return string|null @see $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string|null $category @see $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string|null @see $referenceSignature
     */
    public function getReferenceSignature()
    {
        return $this->referenceSignature;
    }

    /**
     * @param string|null $referenceSignature @see $referenceSignature
     * @return $this
     */
    public function setReferenceSignature($referenceSignature)
    {
        $this->referenceSignature = $referenceSignature;
        return $this;
    }

    /**
     * @return string|null @see $referenceVideoId
     */
    public function getReferenceVideoId()
    {
        return $this->referenceVideoId;
    }

    /**
     * @param string|null $referenceVideoId @see $referenceVideoId
     * @return $this
     */
    public function setReferenceVideoId($referenceVideoId)
    {
        $this->referenceVideoId = $referenceVideoId;
        return $this;
    }



}