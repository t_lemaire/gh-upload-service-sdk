<?php

namespace GorillaHub\SDKs\UploadBundle\Utils;


class FileUploadedResultFactoryTest extends \PHPUnit_Framework_TestCase
{

	public function testGettingInstance()
	{
		$cookies = array(
			'type:cookies',
			'tag:?�K????��????',
		);

		$get  = array(
			'type:get',
			'tag:?�K????��????',
		);
		$post = array(
			'type:post',
			'tag:?�K????��????',
		);

		$result = FileUploadedResultFactory::getInstance($post, $get, $cookies);

		$this->assertEquals($cookies, Base64Encoder::decode($result->getCookies()));
		$this->assertEquals($get, Base64Encoder::decode($result->getGet()));
		$this->assertEquals($post, Base64Encoder::decode($result->getPost()));

	}

	public function testGettingInstanceFromUTF8Character()
	{
		$cookies = array(
			'type:cookies',
			'tag:1',
		);

		$get  = array(
			'type:get',
			'tag:2',
		);
		$post = array(
			'type:post',
			'tag:3',
		);

		$result = FileUploadedResultFactory::getInstance($post, $get, $cookies);

		$this->assertEquals($cookies, $result->getCookies());
		$this->assertEquals($get, $result->getGet());
		$this->assertEquals($post, $result->getPost());

	}

} 
