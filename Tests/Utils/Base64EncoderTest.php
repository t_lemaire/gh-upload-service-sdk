<?php

namespace GorillaHub\SDKs\UploadBundle\Utils;

use \GorillaHub\SDKs\UploadBundle\Utils;

class Base64EncoderTest extends \PHPUnit_Framework_TestCase
{


	public function testEncodeAndDecode()
	{
		$content = array(
			array("domain"         => ".php.net",
				  "expirationDate" => 1422298599.750364,
				  "hostOnly"       => false,
				  "httpOnly"       => false,
				  "name"           => "COUNTRY",
				  "path"           => "/",
				  "secure"         => false,
				  "session"        => false,
				  "storeId"        => "0",
				  "value"          => "NA%2C162.211.96.53",
				  "id"             => 1,
				  'content'        =>
					  array(
						  array('id' => 100000, 'createdTime' => 1420581710, 'tags' =>
							  array(
								  'progress:inprogress',
								  'client_name:tubescms',
								  'tag:encode',
								  'tag:january',
							  )
						  ),
						  array('id' => 100004, 'createdTime' => 1420581714, 'tags' =>
							  array(
								  'progress:inprogress',
								  'client_name:tubescms',
								  'tag:reencode',
							  )
						  ),
					  ),
			),
			array("domain"         => ".php.net",
				  "expirationDate" => 1453577221.06266,
				  "hostOnly"       => false,
				  "httpOnly"       => false,
				  "name"           => "LAST_LANG",
				  "path"           => "/",
				  "secure"         => false,
				  "session"        => false,
				  "storeId"        => "0",
				  "value"          => "en",
				  "id"             => 2,
				  'content'        =>
					  array(
						  array('id' => 100000, 'createdTime' => 1420581710, 'tags' =>
							  array(
								  'progress:inprogress',
								  'client_name:tubescms',
								  'tag:encode',
								  'tag:january',
							  )
						  ),
						  array('id' => 100003, 'createdTime' => 1420581713, 'tags' =>
							  array(
								  'progress:failed',
								  'client_name:tubescms',
								  'tag:reencode',
							  )
						  ),
						  array('id' => 100004, 'createdTime' => 1420581714, 'tags' =>
							  array(
								  'progress:inprogress',
								  'client_name:tubescms',
								  'tag:?�K????��????',
							  )
						  ),
					  ),)
		);

		$this->assertFalse(json_encode($content));

		$encodedResult = Base64Encoder::encode($content);
		$decodeResult  = Base64Encoder::decode($encodedResult);

		$this->assertEquals($content, $decodeResult);
		$this->assertFalse(json_encode($decodeResult));

	}

} 