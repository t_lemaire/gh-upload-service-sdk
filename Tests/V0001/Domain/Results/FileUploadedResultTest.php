<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results;


use GorillaHub\SDKs\UploadBundle\V0001\Domain\Results\FileUploadedResult;

class FileUploadedResultTest extends \PHPUnit_Framework_TestCase{

    public function testSettingCookies()
    {
        $fileUploadedResult = new FileUploadedResult();
        $cookies=$_COOKIE;
        $fileUploadedResult->setCookies($cookies);
        $this->assertEquals($cookies, $fileUploadedResult->getCookies());
    }

    public function testSettingAFileHash()
    {
        $fileUploadedResult = new FileUploadedResult();
        $fileHash='5a828ca5302b19ae8c7a66149f3e1e98';
        $fileUploadedResult->setFileHash($fileHash);
        $this->assertEquals($fileHash,$fileUploadedResult->getFileHash());
    }

    public function testSettingAPost()
    {
        $fileUploadedResult = new FileUploadedResult();
        $post=$_POST;
        $fileUploadedResult->setPost($post);
        $this->assertEquals($post,$fileUploadedResult->getPost());
    }
} 