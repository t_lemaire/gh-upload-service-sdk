<?php

namespace GorillaHub\SDKs\UploadBundle\V0001\Domain;

class ImageOperationTest extends \PHPUnit_Framework_TestCase
{
	public function testSettingFileNameValue()
	{
		$expected = 'filename.jpg';

		$operation = new ImageOperation();

		$operation->setFileName($expected);
		$this->assertEquals($expected, $operation->getFileName());
	}
}