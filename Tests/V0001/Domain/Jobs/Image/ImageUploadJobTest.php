<?php
namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Jobs\Image;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Image\ImageUploadJob;

class ImageUploadJobTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingCallbackValue()
	{
		$expected = 'http://callback';

		$job = new ImageUploadJob();

		$job->setCallbackUrl($expected);
		$this->assertEquals($expected, $job->getCallbackUrl());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid callback url format.
	 */
	public function testSettingCallbackUrlValueException()
	{
		$job = new ImageUploadJob();

		try {
			$job->setCallbackUrl(null);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

}