<?php
namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Jobs\Video;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\FlipBookMetrics;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\EncodeOperation;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\FlipBookOperation;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Video\VideoUploadJob;

class VideoUploadJobTest extends \PHPUnit_Framework_TestCase
{

	public function testConstructorInitialization()
	{
		$job = new VideoUploadJob();

		$this->assertNotEmpty($job->getValidConditions());
	}

	public function testAddingAnOperationWithoutFilePattern()
	{
		$encodeOperation = new EncodeOperation();
		$videoUploadJob  = new VideoUploadJob();
		try {
			$videoUploadJob->addOperation($encodeOperation);
		} catch (InvalidParameterException $e) {
			return;
		}
		$this->fail('Invalid operation content, File Pattern definition is missing for operation: ' . get_class($encodeOperation) . ', id:' . $encodeOperation->getOperationId());
	}

	public function testAddingAnOperationWithFilePattern()
	{
		$encodeOperation = new EncodeOperation();
		$videoUploadJob  = new VideoUploadJob();
		$filePattern     = new FilePattern();
		$filePattern
			//->setBasePath('originals/')
			->setFilePattern('%f.%x');
		$encodeOperation->setFilePattern($filePattern);
		try {
			$videoUploadJob->addOperation($encodeOperation);
		} catch (InvalidParameterException $e) {
			$this->fail('Invalid operation content, Base Path definition for File Pattern for operation: ' . get_class($encodeOperation) . ', id:' . $encodeOperation->getOperationId());
		}
	}

	public function testAddingAnOperation()
	{
		$videoUploadJob = new VideoUploadJob();

		$encodeOperation = new EncodeOperation();
		$filePattern     = new FilePattern();
		$filePattern
			->setBasePath('videos/')
			->setFilePattern('%f.%x');
		$encodeOperation->setFilePattern($filePattern);

		$videoUploadJob->addOperation($encodeOperation);

		$this->assertInstanceOf('\GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation', $encodeOperation);
		$this->assertFalse($encodeOperation instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface);
		$this->assertEquals($encodeOperation, $videoUploadJob->getOperations()[0]);
	}

	public function testSettingOperations()
	{
		$videoUploadJob = new VideoUploadJob();

		$encodeOperation = new EncodeOperation();
		$filePattern     = new FilePattern();
		$filePattern
			->setBasePath('videos/')
			->setFilePattern('%f.%x');
		$encodeOperation->setFilePattern($filePattern);

		$flipBookOperation = new FlipBookOperation();

		$flipBookMetrics = new FlipBookMetrics();

		$filePattern = new FilePattern();
		$filePattern
			->setBasePath('originals/')
			->setFilePattern('%f.%x');
		$flipBookMetrics->setFilePattern($filePattern);
		$flipBookOperation->addMetric($flipBookMetrics);
		$operations = array($encodeOperation, $flipBookOperation);

		$videoUploadJob->setOperations($operations, $videoUploadJob->getOperations());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage The file pattern definition has to be unique in the job.
	 */
	public function testAddingOperationsWithSameFilePattern()
	{
		$videoUploadJob = new VideoUploadJob();

		$filePattern     = new FilePattern();
		$filePattern
			->setBasePath('videos/')
			->setFilePattern('%f.%x');

		$encodeOperation1 = new EncodeOperation();
		$encodeOperation1->setFilePattern($filePattern);

		$videoUploadJob->addOperation($encodeOperation1);

		$encodeOperation2 = new EncodeOperation();
		$encodeOperation2->setFilePattern($filePattern);

		$videoUploadJob->addOperation($encodeOperation2);
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid download video url format.
	 */
	public function testAddingBadDownloadVideoUrl(){
		$job = new VideoUploadJob();
		$job->setDownloadVideoUrl('bad');
	}

	public function testAddingGoodDownloadVideoUrl(){
		$job = new VideoUploadJob();
		$job->setDownloadVideoUrl('http://www.mindgeek.com');
		$this->assertEquals('http://www.mindgeek.com', $job->getDownloadVideoUrl());
	}
}