<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Calls;


use GorillaHub\SDKs\UploadBundle\V0001\Domain\Calls\FileUploadedCall;

class FileUploadedCallTest extends \PHPUnit_Framework_TestCase{

    public function testSettingACallbackUrl()
    {
        $fileUploadedCall = new FileUploadedCall();
        $callbackUrl = 'http://123/callback';
        $fileUploadedCall->setCallbackUrl($callbackUrl);
        $this->assertEquals($callbackUrl, $fileUploadedCall->getCallbackUrl());
    }

} 