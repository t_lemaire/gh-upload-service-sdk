<?php

namespace GorillaHub\SDKs\UploadBundle\Utils;

use GorillaHub\SDKs\UploadBundle\V0001\Domain\Results\FileUploadedResult;

class FileUploadedResultFactory
{

	static function getInstance($post, $get, $cookie)
	{
		$result = new FileUploadedResult();

		$postResult = array();

		foreach ($post as $index => $value) {
			$postResult[$index] = self::getValue($value);
		}

		$result->setPost($postResult);

		$getResult = array();

		foreach ($get as $index => $value) {
			$getResult[$index] = self::getValue($value);
		}

		$result->setGet($getResult);

		$cookieResult = array();

		foreach ($cookie as $index => $value) {
			$cookieResult[$index] = self::getValue($value);
		}

		$result->setCookies($cookieResult);

		return $result;
	}

	static protected function getValue($value)
	{
		if (is_array($value)) {
			$resultedValue = array();
			foreach ($value as $index => $itemValue) {
				$resultedValue[$index] = self::getValue($itemValue);
			}

			return $resultedValue;
		} else {
			$isUtf8 = mb_detect_encoding($value, 'UTF-8', true);

			if ($isUtf8 !== false) {
				return $value;
			}

			return base64_encode($value);
		}
	}

} 
