<?php

namespace GorillaHub\SDKs\UploadBundle\Utils;


class Base64Encoder
{

	/**
	 * @param Array $content
	 *
	 * @return Array
	 */
	public static function encode(array $content)
	{
		$result = array();

		foreach ($content as $key => $value) {
			if (is_array($value)) {
				$result[$key] = self::encode($value);
			} elseif (is_string($value)) {
				$result[$key] = base64_encode($value);
			} else {
				$result[$key] = $value;
			}
		}

		return $result;
	}

	/**
	 * @param Array $content
	 *
	 * @return Array
	 */
	public static function decode(array $content)
	{
		$result = array();

		foreach ($content as $key => $value) {
			if (is_array($value)) {
				$result[$key] = self::decode($value);
			} elseif (is_string($value)) {
				$isBase64Encoded = (base64_encode(base64_decode($value, true)) == $value);
				$result[$key] = $isBase64Encoded ? base64_decode($value) : $value;
			} else {
				$result[$key] = $value;
			}
		}

		return $result;
	}

} 
